# Level 3

## Observations

1. The programs simply repeat the given input and then exits.
2. The program uses `printf()` without any format string.
3. The program compares a global variable `m` to value `0x40 (64)` and spawns an elevated shell if they match. 

## Plan

We will use use `printf()`'s *Format String Attack* to change the value of a given address using the `%n` format operator.

When `printf()` is called with a format string and arguments said arguments are pushed on the stack and `printf()` just needs to go down the stack to get them one by one and create a formatted string. 

But if `printf()` is called with a user-controled buffer as its sole argument and that buffer contains format operators like `%d` or `%x` then `printf()` assumes it's been given arguments and start going down the stack. 

The user is then able to read the entire stack and even, with some clever tricks, to overwrite the value found at any given address thanks to the `%n` format operator.

To be able to pull that off we first need to find how far in the stack our input buffer is when `printf()` is being called :
```bash
python -c 'print "AAAA" + " %08x" * 8' | ./level3
AAAA 00000200 b7fd1ac0 b7ff37d0 41414141 38302520 30252078 25207838 20783830
```
By putting multiple `%08x` in the input buffer and by making `printf()` interpret them as arguments, we are able to read the state of the stack formatted as groups of `4` bytes hexadecimal values.  
We notice that the start of our input (`"AAAA" <=> 0x41414141`) is in the **4th group**.


Then we need to find the address of the value we want to override, in our case the global value `m`.  
`nm` can do it for us easily :
```bash
nm ./level3
[...]
0804988c B m
```
Our target is the address **0x0804988c**.

### The %n Format Operator

Notice this `printf()` call : 
```C
printf("I wrote %n characters", &variable)
```
After `printf()` is done, the integer `variable` will contain the value `8` because `printf()` had written 8 characters when it encountered the `%n` operator.  
`&variable` is an address and it is on top of the stack when `printf()` encounters the `%n` operator, so if we are able to manipulate the stack to put the address we want on top of it when `printf()` encounters a `%n` operator **and** make it so `printf()` wrote a very specific amount of characters at this very moment, we can change the value pointed by this address to whatever we want.

## Payload

We want to write value `0x40 (64)` at address **0x0804988c**.

```bash
(python -c 'print "\x08\x04\x98\x8c"[::-1] + "%60d%4$n"'; cat -) | ./level3
```

`%60d` is simple trick to make printf write *exactly* 60 characters using the decimal width.  
Since we already wrote 4 bytes with the address, we only need 60 more to reach 64 written characters.

The `4$` in `%4$n` tells `printf()` we want to apply this operator to the 4th argument ; since no arguments were given, `printf()` will apply the `%n` argument to the **4th group** of 4 bytes values, which happens to be the start of our string and the address of `m`.