#include <iostream>
#include <cstring>

class N {
public:

        N(int number) : _number(number) {
        }

		void setAnnotation(char *a)
		{
			memcpy(this->_annotation, a, strlen(a));
		}
		
        int operator+(N &r)
		{
			return (this->_number - r._number);
		}
		
        int operator- (N &r)
		{
			return (this->_number - r._number);
		}

private:
		char _annotation[100];
		int _number;
};


int main(int argc, char **argv)
{
	if (argc < 2)
		exit(1);
	
    N *n1 = new N(5);
	N *n2 = new N(6);
	
    n1->setAnnotation(argv[1]);
	
    return (*n1 + *n2);
}