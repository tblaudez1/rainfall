# Level 08

## Observations

1. The program always display the address contained in two global variables `auth` and `service` (both NULL at start).
2. The program reacts when given certain commands found in the program's string table : `auth`, `service`, `reset` and `login`. These commands change the value of the global variables.
3. At some points, the program  spawns an elevated shell but only under certain conditions.

## Plan

We will use the given commands to manipulate the state of the program and trigger the condition that will spawn the elevated shell.

Couple more things to know :

- The condition for the program to spawn a shell is that `auth[32]` be non-zero but `auth` is only allocated 4 bytes.  
- The `service` command fills the `service` global variable using `strdup(input)`.
- When allocated, `auth` and `service` are placed next to each other (with some padding) on the heap.

We can make `auth[32]` be non-zero by allocating `service` next to it with a long enough string (16 bytes at least).

## Payload

```bash
(printf "auth tblaudez\nservice 0123456789abcedf\nlogin\n" ; cat -) | ./level8
```