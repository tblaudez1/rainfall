# Level 4

## Obervations

1. The programs simply repeat the given input and then exits.
2. The program uses `printf()` without any format string.
3. The program compares a global variable `m` to value `0x1025544 (16930116)` and prints the next level's flag if they match.

## Plan

This is exactly the same situation as *level3* just with a much higher value.  
But said value is still under *INT32_MAX* so we can re-use the same `%d` trick. 

- The address of `m` is **0x08049810**.  
- The start of our buffer is in the **12th group**.

## Payload

```bash
(python -c 'print "\x08\x04\x98\x10"[::-1] + "%16930112d%12$n"'; cat -) | ./level4
```