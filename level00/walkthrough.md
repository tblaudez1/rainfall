# Level0

## Observations

The program segfaults immediately after execution when no argument is given.  
When given an arbitrary argument, the program outputs `No!`.  
Looking at the program's instructions, we see that the input is casted to an integer using the `atoi()` function before being compared to the value `0x1a7 (423)`.  
If the input matches this value, the programs spawns a shell with elevated privileges.

## Plan

Give the program the value 423 and use the elevated shell to access the flag.

## Payload

```bash
./level0 423
```