# Level 6

## Obervation

1. The programs segfaults when given no argument and outputs `Nope!` when given one.
2. There is an `n()` function that displays the flag but it is never called.
3. The input is copied in a 64 bytes `malloc()`-ed buffer using `strcpy()`.

## Plan

We will take advantage of `strcpy()`'s lack of size check to create a *Heap Buffer Overflow* and rewrite EIP to the address of `n()`.  

- Using a [*Buffer Overflow Pattern Generator*](https://wiremask.eu/tools/buffer-overflow-pattern-generator), we determine that we reach EIP after **72 bytes**.  
- Using `nm`, we find out that the address of `n()` is **0x08048454**.

## Payload

```bash
./level6 $(python -c 'print "A" * 72 + "\x08\x04\x84\x54"[::-1]')
```