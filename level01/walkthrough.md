# Level 1

## Observations

The program takes an input but nothing seems to happen when one is given.  
Looking at the program's instruction we can see 2 things :

1. The program uses `gets()` to put the input in a buffer.
2. A function named `run()` that is never called can spawn an elevated shell.

## Plan

`gets()` is vulnerable to *Buffer Overflows* because it does not check the size of the buffer it's writing into.  
We can use that *Buffer Overflow* to rewrite EIP and force `gets()` to return to `run()` instead of main.

### Step 1
First we need to know how many bytes to write in the buffer to reach EIP using a [Buffer Overflow Pattern Generator](https://wiremask.eu/tools/buffer-overflow-pattern-generator).  
We can use this long string to make the program segfault because a *Buffer Overflow* will rewrite EIP and thus the return address.  
EIP now contains 4 bytes of the string, we now know exaclty how many bytes we need to reach EIP : **76 bytes**. 

### Step 2
Then we need to find the address of the `run()` function, a simple call to `nm` can give us this information : **0x08048444**.

Now if we manage to rewrite EIP to this exact value, we can get our elevated shell.

## Payload

```bash
(python -c 'print "A" * 76 + "\x08\x04\x84\x44"[::-1]'; cat -) | ./level1
```

We use Python to easily print a number of arbitrary character and convert the address to Little Endian with `[::-1]`.  
The whole parenthesis, `cat -` and pipe stuff is a trick to force STDIN to stay open after we spawn a shell.