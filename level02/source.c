#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void p() {
  char tab[76];
  size_t eip = (size_t)(&tab + 80);

  fflush(stdout);
  gets(tab);

  if ((eip & 0xb0000000) == 0xb0000000)
  {
    printf("(%p)\n", (void *)eip);
    exit(1);
  }

  strdup(tab);
  puts(tab);
}

int main() {
  p();
  return 0;
}